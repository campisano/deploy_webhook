binary_name			:= deploy-webhook
type				:= exec

include_paths			:= src/application/inc src/adapters/inc \
                                   src/cmpcom/inc src/cmpsys/inc \
                                   lib/doctest/install/include
library_paths			:= src/domain/build/lib src/application/build/lib src/adapters/build/lib \
                                   src/cmpcom/build/lib src/cmpsys/build/lib

source_paths			:= src
libs_to_link			:= :libcmpcom.so :libcmpsys.so \
                                   :libdomain.so :libapplication.so :libadapters.so \
                                   spdlog yaml-cpp ssl

main_source_paths		:= src/main

test_source_paths		:= src/test

PREFIX				:= $(CURDIR)/install

format_dirs			:= src

subcomponent_paths		:= src/cmpcom src/cmpsys \
                                   src/domain src/application src/adapters

include lib/generic_makefile/modular_makefile.mk

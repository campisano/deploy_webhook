[![Build Status](https://gitlab.com/campisano/deploy_webhook/badges/master/pipeline.svg "Build Status")](https://gitlab.com/campisano/deploy_webhook/-/pipelines)
[![Sonar Coverage](https://sonarcloud.io/api/project_badges/measure?project=deploy_webhook&metric=coverage "Sonar Coverage")](https://sonarcloud.io/dashboard?id=deploy_webhook)
[![Sonar Maintainability](https://sonarcloud.io/api/project_badges/measure?project=deploy_webhook&metric=sqale_rating "Sonar Maintainability")](https://sonarcloud.io/dashboard?id=deploy_webhook)
[![Docker Image](https://img.shields.io/docker/image-size/riccardocampisano/public/deploy_webhook-latest?label=deploy_webhook-latest&logo=docker "Docker Image")](https://hub.docker.com/r/riccardocampisano/public/tags?name=deploy_webhook)

# TODO

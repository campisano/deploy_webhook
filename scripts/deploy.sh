#!/bin/bash
#

show_usage()
{
    echo "Usage:    "`basename $0`" <protocol://host:port> <stack-name> <bearer> <stack-file-path>"
    echo "Example:  "`basename $0`" http://127.0.0.1:8080 my-stack xxx-yyy-zzz resources/stack.yml"
}

if test $# -ne 4
then
    show_usage >&2
    exit 1
fi

BASE_URL="$1"
STACK_NAME="$2"
BEARER="$3"
STACK="$(cat $4)"

URL="${BASE_URL}/v1/deploy/${STACK_NAME}"

set -x
curl -q -sSw "\n\nHTTP code: %{http_code}\n" \
-k --ciphers 'DEFAULT:!DH' \
-X POST \
"${URL}" \
-H "Cache-Control: no-cache" \
-H "Content-Type: application/json" \
-H "Authorization: Bearer ${BEARER}" \
-d "${STACK}"



# End

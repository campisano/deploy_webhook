#include "cmpcom/strings.hpp"

#include <algorithm>

namespace cmp
{
namespace str
{
std::string toLower(const std::string & _string)
{
    std::string lcase;
    lcase.resize(_string.size());
    std::transform(_string.begin(), _string.end(), lcase.begin(), ::tolower);

    return lcase;
}

std::vector<std::string> split(const std::string & _string,
                               const std::string & _delimiter)
{
    std::vector<std::string> result;

    auto start = 0;
    auto end = _string.find(_delimiter);
    auto del_len = _delimiter.length();

    while(end != std::string::npos)
    {
        result.push_back(_string.substr(start, end - start));
        start = end + del_len;
        end = _string.find(_delimiter, start);
    }

    result.push_back(_string.substr(start, end));

    return result;
}
}
}

#ifndef CMP_STR__HPP__
#define CMP_STR__HPP__

#include <string>
#include <vector>

namespace cmp
{
namespace str
{
std::string toLower(const std::string & _string);
std::vector<std::string> split(const std::string & _string,
                               const std::string & _delimiter);
}
}

#endif

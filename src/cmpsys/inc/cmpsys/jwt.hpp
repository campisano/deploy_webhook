#ifndef JWT__HPP__
#define JWT__HPP__

#include <cmpcom/move_only.hpp>
#include <map>
#include <string>

namespace cmp
{
class Jwt: public MoveOnly
{
public:
    explicit Jwt(const std::string & _token);
    virtual ~Jwt();

    void validate(std::string _algorithm, std::string _secret);

    const std::map<std::string, std::string> & header();
    const std::map<std::string, std::string> & payload();
    const std::string & algorithm();
private:
    void * m_data;
};
}

#endif

#ifndef LOGGER__HPP__
#define LOGGER__HPP__

#include <memory>
#include <string>

namespace cmp
{
class Logger
{
public:
    virtual ~Logger() = default;

public:
    virtual std::unique_ptr<Logger> namedLogger(const std::string & _name) = 0;

    virtual Logger & debug(const std::string & _message) = 0;
    virtual Logger & info(const std::string & _message) = 0;
    virtual Logger & warn(const std::string & _message) = 0;
    virtual Logger & error(const std::string & _message) = 0;
};
}

#endif

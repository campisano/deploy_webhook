#ifndef CMP_PROCESS_HPP
#define CMP_PROCESS_HPP

#include <cmpcom/move_only.hpp>
#include <cstdint>
#include <string>

namespace cmp
{
class Process: public MoveOnly
{
public:
    explicit Process(const std::string & _command);
    explicit Process(const std::string & _command, const std::string & _stdin);
    virtual ~Process();

    const std::string & stdout();
    const std::string & stderr();
    std::uint32_t code();

private:
    void * m_data;
};
}

#endif

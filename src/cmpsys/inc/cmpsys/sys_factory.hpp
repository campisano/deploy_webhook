#ifndef SYS_FACTORY__HPP__
#define SYS_FACTORY__HPP__

#include <memory>

#include <cmpsys/logger.hpp>

namespace cmp
{
class SysFactory
{
private:
    explicit SysFactory();

public:
    static std::unique_ptr<Logger> makeLogger(
        const std::string & _format, const std::string & _level);
};
}

#endif

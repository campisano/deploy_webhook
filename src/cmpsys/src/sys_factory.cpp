#include <cmpsys/sys_factory.hpp>

#include "logger/spdlog_logger.hpp"

namespace cmp
{
SysFactory::SysFactory()
{
}

std::unique_ptr<Logger> SysFactory::makeLogger(const std::string & _format,
        const std::string & _level)
{
    return std::unique_ptr<Logger>(new SpdlogLogger(_format, _level));
}
}

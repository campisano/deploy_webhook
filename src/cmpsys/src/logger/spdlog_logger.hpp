#ifndef SPDLOG_LOGGER__HPP__
#define SPDLOG_LOGGER__HPP__

#include <cmpsys/logger.hpp>
#include <string>

class SpdlogLogger : public cmp::Logger
{
public:
    explicit SpdlogLogger(const std::string & _format, const std::string & _level);
    SpdlogLogger(const SpdlogLogger &) = delete;
    SpdlogLogger(SpdlogLogger &&) = default;
    virtual ~SpdlogLogger();

    SpdlogLogger & operator=(const SpdlogLogger &) = delete;
    SpdlogLogger & operator=(SpdlogLogger &&) = default;

private:
    explicit SpdlogLogger(const std::string & _name);

public:
    std::unique_ptr<Logger> namedLogger(const std::string & _name);

    cmp::Logger & debug(const std::string & _message);
    cmp::Logger & info(const std::string & _message);
    cmp::Logger & warn(const std::string & _message);
    cmp::Logger & error(const std::string & _message);

private:
    std::string m_name;
};

#endif

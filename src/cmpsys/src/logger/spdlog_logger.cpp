#include "spdlog_logger.hpp"
#include "spdlog/common.h"

#include <cctype>
#include <locale>
#include <spdlog/sinks/basic_file_sink.h>
#include <spdlog/sinks/stdout_color_sinks.h>
#include <spdlog/spdlog.h>

namespace
{
std::shared_ptr<spdlog::logger> getLogger(std::string _name);
}

SpdlogLogger::SpdlogLogger(const std::string & _format,
                           const std::string & _level)
    : m_name("root")
{
    std::string lcase_level;
    lcase_level.resize(_level.size());
    std::transform(_level.begin(), _level.end(), lcase_level.begin(), ::tolower);

    auto console_sink = std::make_shared<spdlog::sinks::stdout_color_sink_mt>();
    auto logger = std::make_shared<spdlog::logger>(m_name, console_sink);
    logger->set_level(spdlog::level::from_str(lcase_level));
    logger->set_pattern(_format);
    spdlog::set_default_logger(logger);
}

SpdlogLogger::SpdlogLogger(const std::string & _name)
    : m_name(_name)
{
}

std::unique_ptr<cmp::Logger> SpdlogLogger::namedLogger(const std::string &
        _name)
{
    return std::unique_ptr<cmp::Logger>(new SpdlogLogger(_name));
}

SpdlogLogger::~SpdlogLogger()
{
}

cmp::Logger & SpdlogLogger::debug(const std::string & _message)
{
    getLogger(m_name)->debug(_message);

    return * this;
}

cmp::Logger & SpdlogLogger::info(const std::string & _message)
{
    getLogger(m_name)->info(_message);

    return * this;
}

cmp::Logger & SpdlogLogger::warn(const std::string & _message)
{
    getLogger(m_name)->warn(_message);

    return * this;
}

cmp::Logger & SpdlogLogger::error(const std::string & _message)
{
    getLogger(m_name)->error(_message);

    return * this;
}

namespace
{
std::shared_ptr<spdlog::logger> getLogger(std::string _name)
{
    auto logger = spdlog::get(_name);

    if(!logger)
    {
        logger = spdlog::get("root")->clone(_name);
        spdlog::register_logger(logger);
    }

    return logger;
}
}

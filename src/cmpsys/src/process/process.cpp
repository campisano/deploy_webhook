#include <cmpsys/process.hpp>

#include <pstream.h>

namespace
{
struct Data
{
    std::string stdout;
    std::string stderr;
    std::uint32_t code;
};

void start(Data & data, const std::string & _command,
           const std::string & _stdin);
}

namespace cmp
{
Process::Process(const std::string & _command)
{
    m_data = 0;
    m_data = new Data();
    auto data = static_cast<Data *>(m_data);
    start(*data, _command, "");
}

Process::Process(const std::string & _command, const std::string & _stdin)
{
    m_data = 0;
    m_data = new Data();
    auto data = static_cast<Data *>(m_data);
    start(*data, _command, _stdin);
}

Process::~Process()
{
    if(m_data)
    {
        delete static_cast<Data *>(m_data);
        m_data = 0;
    }
}

const std::string & Process::stdout()
{
    auto data = static_cast<Data *>(m_data);

    return data->stdout;
}

const std::string & Process::stderr()
{
    auto data = static_cast<Data *>(m_data);

    return data->stderr;
}

std::uint32_t Process::code()
{
    auto data = static_cast<Data *>(m_data);

    return data->code;
}
}

namespace
{
void start(Data & data, const std::string & _command,
           const std::string & _stdin)
{
    redi::pstream proc(_command,
                       redi::pstreams::pstdin | redi::pstreams::pstdout | redi::pstreams::pstderr);

    if(! _stdin.empty())
    {
        proc << _stdin << redi::peof;
    }

    while(std::getline(proc.out(), data.stdout, {}));

    // if reading stdout stopped at EOF then reset the state:
    if(proc.eof() && proc.fail())
    {
        proc.clear();
    }

    while(std::getline(proc.err(), data.stderr, {}));

    proc.close();

    data.code = proc.rdbuf()->status();
}
}

#include <cmpsys/jwt.hpp>

#include <cmpcom/strings.hpp>
#include <jwt-cpp/jwt.h>
#include <stdexcept>

namespace
{
struct Data
{
    std::map<std::string, std::string> m_header;
    std::map<std::string, std::string> m_payload;
    std::string m_algorithm;

    // I know, waste of cpu only to have the correct exception translated
    // TODO find a better design or jwt implementation
    std::string m_token;
};
}

cmp::Jwt::Jwt(const std::string & _token)
{
    m_data = 0;
    m_data = new Data();
    auto data = (Data *) m_data;

    data->m_token = _token;

    try
    {
        // I know, waste of cpu only to have the correct exception translated
        // TODO find a better design or jwt implementation
        jwt::decode(_token);
    }
    catch(const std::exception & _ex)
    {
        throw std::runtime_error("wrong token format");
    }

    auto decoded = jwt::decode(_token);
    auto header = decoded.get_header_json();

    for(auto it = header.cbegin(); it != header.cend(); ++it)
    {
        data->m_header[it->first] = it->second.to_str();
    }

    auto payload = decoded.get_payload_json();

    for(auto it = payload.cbegin(); it != payload.cend(); ++it)
    {
        data->m_payload[it->first] = it->second.to_str();
    }

    data->m_algorithm = decoded.get_algorithm().c_str();
}

cmp::Jwt::~Jwt()
{
    if(m_data)
    {
        delete static_cast<Data *>(m_data);
        m_data = 0;
    }
}

void cmp::Jwt::validate(std::string _algorithm, std::string _secret)
{
    auto data = (Data *) m_data;

    // I know, waste of cpu only to have the correct exception translated
    // TODO find a better design or jwt implementation
    auto decoded = jwt::decode(data->m_token);

    std::error_code ec;

    auto verifier = jwt::verify();

    if(cmp::str::toLower(_algorithm) == "hs256")
    {
        verifier.allow_algorithm(jwt::algorithm::hs256{ _secret });
    }
    else
    {
        throw std::runtime_error("bad algorithm: " + _algorithm);
    }

    verifier.verify(decoded, ec);

    if(ec)
    {
        throw std::runtime_error(ec.message());
    }
}


const std::map<std::string, std::string> & cmp::Jwt::header()
{
    auto data = (Data *) m_data;

    return data->m_header;
}

const std::map<std::string, std::string> & cmp::Jwt::payload()
{
    auto data = (Data *) m_data;

    return data->m_payload;
}

const std::string & cmp::Jwt::algorithm()
{
    auto data = (Data *) m_data;

    return data->m_algorithm;
}

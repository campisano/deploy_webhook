#include <adapters/adapters_factory.hpp>
#include <application/application_factory.hpp>
#include <cmpsys/sys_factory.hpp>

int main()
{
    auto config_ldr = AdaptersFactory::makeConfigProvider("env");
    auto config = config_ldr->config();

    auto logger = cmp::SysFactory::makeLogger(config.logger.format, config.logger.level);

    auto deployer = AdaptersFactory::makeDeployer(*logger, config.deploy.command);
    auto deploy_uc = ApplicationFactory::makeDeployUsecase(*deployer);

    auto http_srv = AdaptersFactory::makeHTTPServer(*logger);
    auto health_cnt = AdaptersFactory::makeHTTPHealthCheckController(*http_srv, *logger);

    auto deploy_cnt = AdaptersFactory::makeHTTPDeployController(
        *http_srv, config.auth.algorithm, config.auth.secret, *deploy_uc, *logger);

    http_srv->start(config.http.host, config.http.port, config.http.threads);
    http_srv->wait();

    return 0;
}

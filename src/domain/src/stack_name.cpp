#include <domain/stack_name.hpp>

#include <stdexcept>

namespace
{
void ensureValidContent(const std::string & _content);
}

StackName::StackName(const std::string & _content)
{
    ensureValidContent(_content);
    m_content = _content;
}

StackName::~StackName()
{
}

const std::string & StackName::content() const
{
    return m_content;
}

namespace
{
void ensureValidContent(const std::string & _content)
{
    if(_content.size() == 0)
    {
        throw std::runtime_error("The stack name is invalid:\n'" + _content);
    }
}
}

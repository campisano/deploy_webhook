#include <domain/stack.hpp>

#include <stdexcept>

namespace
{
void ensureValidContent(const std::string & _content);
}

Stack::Stack(const std::string & _content)
{
    ensureValidContent(_content);
    m_content = _content;
}

Stack::~Stack()
{
}

const std::string & Stack::content() const
{
    return m_content;
}

namespace
{
void ensureValidContent(const std::string & _content)
{
    if(_content.size() == 0)
    {
        throw std::runtime_error("The stack is invalid:\n'" + _content);
    }
}
}

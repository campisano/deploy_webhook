#ifndef STACK_NAME__HPP__
#define STACK_NAME__HPP__

#include <string>

class StackName
{
public:
    explicit StackName(
        const std::string & _content);
    StackName(const StackName &) = delete;
    StackName(StackName &&) = default;
    virtual ~StackName();

    StackName & operator=(const StackName &) = delete;
    StackName & operator=(StackName &&) = default;

public:
    const std::string & content() const;

private:
    std::string m_content;
};

#endif

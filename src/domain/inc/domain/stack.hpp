#ifndef STACK__HPP__
#define STACK__HPP__

#include <string>

class Stack
{
public:
    explicit Stack(
        const std::string & _content);
    Stack(const Stack &) = delete;
    Stack(Stack &&) = default;
    virtual ~Stack();

    Stack & operator=(const Stack &) = delete;
    Stack & operator=(Stack &&) = default;

public:
    const std::string & content() const;

private:
    std::string m_content;
};

#endif

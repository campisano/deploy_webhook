#include "doctest.h"

#include <stdexcept>
#include "domain/stack.hpp"

TEST_SUITE_BEGIN("StackTG");

TEST_CASE("when_create_new_then_new_returns")
{
    Stack s("aaa");
}

TEST_CASE("when_create_empty_stack_then_throw_exception")
{
    CHECK_THROWS_AS(Stack(""), std::runtime_error);
}

TEST_CASE("when_get_content_then_content_returns")
{
    Stack s("some");

    CHECK_EQ(std::string("some"), s.content());
}

TEST_SUITE_END();

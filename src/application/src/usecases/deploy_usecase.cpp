#include "deploy_usecase.hpp"

#include <memory>
#include <stdexcept>

#include <application/exceptions/deploy_error_exception.hpp>
#include <application/exceptions/invalid_deploy_exception.hpp>
#include <domain/stack.hpp>
#include <domain/stack_name.hpp>

DeployUsecase::DeployUsecase(const DeployerPort & _deployer)
    : m_deployer(_deployer)
{
}

DeployUsecase::~DeployUsecase()
{
}

std::string DeployUsecase::execute(const std::string & _stack_name,
                                   const std::string & _stack)
{
    // TODO: invalid deploy exception?
    StackName evaluated_stack_name(_stack_name);
    Stack evaluated_stack(_stack);

    try
    {
        return m_deployer.deploy(evaluated_stack_name.content(),
                                 evaluated_stack.content());
    }
    catch(const std::exception & _exc)
    {
        throw DeployErrorException(_exc.what());
    }
}

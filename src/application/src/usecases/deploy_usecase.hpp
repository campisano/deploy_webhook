#ifndef DEPLOY_USECASE__HPP__
#define DEPLOY_USECASE__HPP__

#include <application/ports/deployer_port.hpp>
#include <application/ports/usecases/deploy_usecase_port.hpp>

class DeployUsecase : public DeployUsecasePort
{
public:
    explicit DeployUsecase(const DeployerPort & _deployer);
    DeployUsecase(const DeployUsecase &) = delete;
    DeployUsecase(DeployUsecase &&) = default;
    virtual ~DeployUsecase();

    DeployUsecase & operator=(const DeployUsecase &) = delete;
    DeployUsecase & operator=(DeployUsecase &&) = default;

public:
    virtual std::string execute(const std::string & _stack_name,
                                const std::string & _stack);

private:
    const DeployerPort & m_deployer;
};

#endif

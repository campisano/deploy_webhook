#include <application/application_factory.hpp>

#include "usecases/deploy_usecase.hpp"

ApplicationFactory::ApplicationFactory()
{
}

std::unique_ptr<DeployUsecasePort>
ApplicationFactory::makeDeployUsecase(const DeployerPort & _deployer)
{
    return std::unique_ptr<DeployUsecasePort>(
               new DeployUsecase(_deployer));
}

#ifndef INVALID_DEPLOY_EXCEPTION__HPP__
#define INVALID_DEPLOY_EXCEPTION__HPP__

#include <stdexcept>
#include <string>

class InvalidDeployException : public std::runtime_error
{
public:
    virtual ~InvalidDeployException() = default;

public:
    explicit InvalidDeployException(const std::string & _message)
        : std::runtime_error(_message)
    {
    }
};

#endif

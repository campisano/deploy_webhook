#ifndef DEPLOY_ERROR_EXCEPTION__HPP__
#define DEPLOY_ERROR_EXCEPTION__HPP__

#include <stdexcept>
#include <string>

class DeployErrorException : public std::runtime_error
{
public:
    virtual ~DeployErrorException() = default;

public:
    explicit DeployErrorException(const std::string & _message)
        : std::runtime_error(_message)
    {
    }
};

#endif

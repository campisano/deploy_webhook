#ifndef APPLICATION_FACTORY__HPP__
#define APPLICATION_FACTORY__HPP__

#include <memory>

#include <application/ports/deployer_port.hpp>
#include <application/ports/usecases/deploy_usecase_port.hpp>

class ApplicationFactory
{
private:
    explicit ApplicationFactory();

public:
    static std::unique_ptr<DeployUsecasePort> makeDeployUsecase(
        const DeployerPort & _deployer);
};

#endif

#ifndef DEPLOYER_PORT__HPP__
#define DEPLOYER_PORT__HPP__

#include <string>

class DeployerPort
{
public:
    virtual ~DeployerPort() = default;

public:
    virtual std::string deploy(const std::string & _stack_name,
                               const std::string & _stack) const = 0;
};

#endif

#ifndef DEPLOY_USECASE_PORT__HPP__
#define DEPLOY_USECASE_PORT__HPP__

#include <string>

class DeployUsecasePort
{
public:
    virtual ~DeployUsecasePort() = default;

public:
    virtual std::string execute(const std::string & _stack_name,
                                const std::string & _stack) = 0;
};

#endif

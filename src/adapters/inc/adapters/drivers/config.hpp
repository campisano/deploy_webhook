#ifndef CONFIG__HPP__
#define CONFIG__HPP__

#include <string>

struct Config
{
    struct Auth
    {
        std::string algorithm;
        std::string secret;
    } auth;

    struct Http
    {
        std::string host;
        int port;
        int threads;
    } http;

    struct Logger
    {
        std::string format;
        std::string level;
    } logger;

    struct Deploy
    {
        std::string command;
    } deploy;
};

#endif

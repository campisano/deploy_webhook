#ifndef ADAPTERS_FACTORY__HPP__
#define ADAPTERS_FACTORY__HPP__

#include <adapters/controllers/http_controller.hpp>
#include <adapters/drivers/config_provider.hpp>
#include <adapters/drivers/http_server.hpp>
#include <application/ports/deployer_port.hpp>
#include <application/ports/usecases/deploy_usecase_port.hpp>
#include <cmpsys/logger.hpp>
#include <memory>
#include <string>

class AdaptersFactory
{
private:
    explicit AdaptersFactory();

public:
    static std::unique_ptr<ConfigProvider> makeConfigProvider(
        const std::string & _driver);

    static std::unique_ptr<DeployerPort> makeDeployer(
        cmp::Logger & _logger,
        const std::string & _command);

    static std::unique_ptr<HTTPServer> makeHTTPServer(cmp::Logger & _logger);

    static std::unique_ptr<HTTPController> makeHTTPHealthCheckController(
        HTTPServer & _server,
        cmp::Logger & _logger);
    static std::unique_ptr<HTTPController> makeHTTPDeployController(
        HTTPServer & _server,
        const std::string & _algorithm,
        const std::string & _secret,
        DeployUsecasePort & _usecase,
        cmp::Logger & _logger);
};

#endif

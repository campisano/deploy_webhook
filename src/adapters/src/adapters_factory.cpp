#include <adapters/adapters_factory.hpp>

#include <sstream>
#include <stdexcept>
#include "controllers/httplib_http_deploy_controller.hpp"
#include "controllers/httplib_http_health_check_controller.hpp"
#include "drivers/deploy/command_line_deployer.hpp"
#include "drivers/http/httplib_http_server.hpp"
#include "drivers/config/env_config_provider.hpp"
#include "drivers/config/json_config_provider.hpp"
#include "drivers/config/yaml_config_provider.hpp"

AdaptersFactory::AdaptersFactory()
{
}

std::unique_ptr<ConfigProvider> AdaptersFactory::makeConfigProvider(
    const std::string & _source)
{
    if(_source == "env")
    {
        return std::unique_ptr<ConfigProvider>(new EnvConfigProvider());
    }

    auto ext = _source.substr(_source.find_last_of(".") + 1);

    if(ext == "json")
    {
        return std::unique_ptr<ConfigProvider>(new JsonConfigProvider(_source));
    }

    if(ext == "yaml" || ext == "yml")
    {
        return std::unique_ptr<ConfigProvider>(new YamlConfigProvider(_source));
    }

    std::stringstream msg;
    msg << "Specified config source is invalid: " << _source;
    throw std::runtime_error(msg.str());
}

std::unique_ptr<DeployerPort> AdaptersFactory::makeDeployer(
    cmp::Logger & _logger, const std::string & _command)
{
    return std::unique_ptr<DeployerPort>(new CommandLineDeployer(_logger,
                                         _command));
}

std::unique_ptr<HTTPServer> AdaptersFactory::makeHTTPServer(
    cmp::Logger & _logger)
{
    return std::unique_ptr<HTTPServer>(new HttplibHTTPServer(_logger));
}

std::unique_ptr<HTTPController> AdaptersFactory::makeHTTPHealthCheckController(
    HTTPServer & _server, cmp::Logger & _logger)
{
    return std::unique_ptr<HTTPController>(
               new HttplibHTTPHealthCheckController(_server, _logger));
}

std::unique_ptr<HTTPController> AdaptersFactory::makeHTTPDeployController(
    HTTPServer & _server,
    const std::string & _algorithm,
    const std::string & _secret,
    DeployUsecasePort & _usecase,
    cmp::Logger & _logger)
{
    return std::unique_ptr<HTTPController>(
               new HttplibHTTPDeployController(_server, _algorithm, _secret, _usecase,
                       _logger));
}

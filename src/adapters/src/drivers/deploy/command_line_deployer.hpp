#ifndef COMMAND_LINE_DEPLOYER__HPP__
#define COMMAND_LINE_DEPLOYER__HPP__

#include <application/ports/deployer_port.hpp>
#include <cmpsys/logger.hpp>
#include <memory>

class CommandLineDeployer : public DeployerPort
{
public:
    explicit CommandLineDeployer(cmp::Logger & _logger,
                                 const std::string & _command);
    virtual ~CommandLineDeployer() = default;

public:
    std::string deploy(const std::string & _stack_name,
                       const std::string & _stack) const;

private:
    std::string m_command;
    std::unique_ptr<cmp::Logger> m_logger;

};

#endif

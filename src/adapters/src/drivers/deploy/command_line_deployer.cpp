#include "command_line_deployer.hpp"
#include <cctype>
#include <cmpsys/process.hpp>
#include <stdexcept>

CommandLineDeployer::CommandLineDeployer(cmp::Logger & _logger,
        const std::string & _command)
    : m_command(_command), m_logger(_logger.namedLogger(__FILE__))
{
}

std::string CommandLineDeployer::deploy(const std::string & _stack_name,
                                        const std::string & _stack) const
{
    auto cmd_with_env_var = "export STACK_NAME=" + _stack_name + "; " + m_command;
    cmp::Process p(cmd_with_env_var, _stack);

    if(!p.stderr().empty())
    {
        auto s = p.stderr();

        if(s.back() == '\n')
        {
            s.pop_back();
        }

        throw std::runtime_error(s);
    }

    if(p.code() != 0)
    {
        throw std::runtime_error("exit status " + std::to_string(p.code()));
    }

    std::string result;

    if(! p.stdout().empty())
    {
        result = p.stdout();

        if(result.back() == '\n')
        {
            result.pop_back();
        }

        m_logger->debug(std::string("output: ") + result);
    }

    return result;
}

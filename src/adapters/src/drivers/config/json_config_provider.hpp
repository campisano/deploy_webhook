#ifndef JSON_CONFIG_PROVIDER__HPP__
#define JSON_CONFIG_PROVIDER__HPP__

#include <string>
#include <adapters/drivers/config_provider.hpp>

class JsonConfigProvider : public ConfigProvider
{
public:
    explicit JsonConfigProvider(const std::string & _file_path);
    JsonConfigProvider(const JsonConfigProvider &) = delete;
    JsonConfigProvider(JsonConfigProvider &&) = default;
    virtual ~JsonConfigProvider();

    JsonConfigProvider & operator=(const JsonConfigProvider &) = delete;
    JsonConfigProvider & operator=(JsonConfigProvider &&) = default;

public:
    virtual Config config();

private:
    std::string m_file_path;
};

#endif

#include "json_config_provider.hpp"

#include <fstream>
#include <nlohmann/json.hpp>
#include <string>

JsonConfigProvider::JsonConfigProvider(const std::string & _file_path)
    : m_file_path(_file_path)
{
}

JsonConfigProvider::~JsonConfigProvider()
{
}

Config JsonConfigProvider::config()
{
    std::ifstream config_file(m_file_path);
    nlohmann::json j = nlohmann::json::parse(config_file);

    Config::Auth auth_cfg
    {
        j["auth"]["algorithm"].get<std::string>(),
        j["auth"]["secret"].get<std::string>()
    };

    Config::Http http_cfg
    {
        j["http"]["host"].get<std::string>(),
        j["http"]["port"].get<int>(),
        j["http"]["threads"].get<int>()
    };

    Config::Logger log_cfg
    {
        j["logger"]["format"].get<std::string>(),
        j["logger"]["level"].get<std::string>()
    };

    Config::Deploy deploy_cfg
    {
        j["deploy"]["command"].get<std::string>()
    };

    return Config{auth_cfg, http_cfg, log_cfg, deploy_cfg};
}

#include "yaml_config_provider.hpp"

#include <yaml-cpp/yaml.h>
#include <string>

YamlConfigProvider::YamlConfigProvider(const std::string & _file_path)
    : m_file_path(_file_path)
{
}

YamlConfigProvider::~YamlConfigProvider()
{
}

Config YamlConfigProvider::config()
{
    YAML::Node y = YAML::LoadFile(m_file_path);

    Config::Auth auth_cfg
    {
        y["auth"]["algorithm"].as<std::string>(),
        y["auth"]["secret"].as<std::string>()
    };

    Config::Http http_cfg
    {
        y["http"]["host"].as<std::string>(),
        y["http"]["port"].as<int>(),
        y["http"]["threads"].as<int>()
    };

    Config::Logger log_cfg
    {
        y["logger"]["format"].as<std::string>(),
        y["logger"]["level"].as<std::string>()
    };

    Config::Deploy deploy_cfg
    {
        y["deploy"]["command"].as<std::string>()
    };

    return Config{auth_cfg, http_cfg, log_cfg, deploy_cfg};
}

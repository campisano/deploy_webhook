#ifndef YAML_CONFIG_PROVIDER__HPP__
#define YAML_CONFIG_PROVIDER__HPP__

#include <string>
#include <adapters/drivers/config_provider.hpp>

class YamlConfigProvider : public ConfigProvider
{
public:
    explicit YamlConfigProvider(const std::string & _file_path);
    YamlConfigProvider(const YamlConfigProvider &) = delete;
    YamlConfigProvider(YamlConfigProvider &&) = default;
    virtual ~YamlConfigProvider();

    YamlConfigProvider & operator=(const YamlConfigProvider &) = delete;
    YamlConfigProvider & operator=(YamlConfigProvider &&) = default;

public:
    virtual Config config();

private:
    std::string m_file_path;
};

#endif

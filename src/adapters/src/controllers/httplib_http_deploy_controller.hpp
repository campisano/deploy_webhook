#ifndef HTTPLIB_HTTP_DEPLOY_CONTROLLER__HPP__
#define HTTPLIB_HTTP_DEPLOY_CONTROLLER__HPP__

#include <adapters/controllers/http_controller.hpp>
#include <adapters/drivers/http_server.hpp>
#include <application/ports/usecases/deploy_usecase_port.hpp>
#include <cmpsys/logger.hpp>
#include <httplib.h>
#include <memory>

class HttplibHTTPDeployController : public HTTPController
{
public:
    explicit HttplibHTTPDeployController(
        HTTPServer & _server,
        const std::string & _algorithm,
        const std::string & _secret,
        DeployUsecasePort & _usecase,
        cmp::Logger & _logger);
    HttplibHTTPDeployController(const HttplibHTTPDeployController &) = delete;
    HttplibHTTPDeployController(HttplibHTTPDeployController &&) = delete;
    virtual ~HttplibHTTPDeployController();

    HttplibHTTPDeployController & operator=(
        const HttplibHTTPDeployController &) = delete;
    HttplibHTTPDeployController & operator=(
        HttplibHTTPDeployController &&) = delete;

public:
    void deploy(
        const httplib::Request &,
        httplib::Response &);

private:
    std::string m_algorithm;
    std::string m_secret;
    DeployUsecasePort & m_usecase;
    std::unique_ptr<cmp::Logger> m_logger;
};

#endif

#include "httplib_utils.hpp"

#include <cmpsys/jwt.hpp>
#include <cmpcom/strings.hpp>
#include <nlohmann/json.hpp>
#include <string>

void setJSONResponse(
    httplib::Response & _response,
    int _status,
    std::map<std::string, std::string> _body)
{
    _response.status = _status;
    nlohmann::json j = _body;
    _response.set_content(j.dump(), "application/json");
}

std::string getBearerToken(const std::string & _auth_header)
{
    if(_auth_header.empty())
    {
        throw UnauthorizedException("missing authorization");
    }

    auto sp = cmp::str::split(_auth_header, " ");

    if(sp.size() != 2 || cmp::str::toLower(sp[0]) != "bearer")
    {
        throw UnauthorizedException("bad authorization scheme");
    }

    return sp[1];
}

void ensureAuth(const std::string & _token, const std::string & _algorithm,
                const std::string & _secret, cmp::Logger & _logger)
{
    std::map<std::string, std::string> claims;

    try
    {
        cmp::Jwt jwt(_token);

        claims = jwt.payload();
        auto msg = std::string("{");

        for(auto it = claims.cbegin(); it != claims.cend(); ++it)
        {
            if(it != claims.begin())
            {
                msg += ", ";
            }

            msg += it->first + ": " + it->second;
        }

        msg += "}";
        _logger.debug("token claims: " + msg);

        jwt.validate(_algorithm, _secret);
    }
    catch(const std::exception & _exc)
    {
        throw UnauthorizedException("invalid token");
    }

    if((! claims.count("iss")) || claims["iss"] != "auth.campisano.org")
    {
        throw ForbiddenException("bad issuer");
    }

    if((! claims.count("aud")) || claims["aud"] != "deploy.campisano.org")
    {
        throw ForbiddenException("bad audience");
    }

    if((! claims.count("scope")) || claims["scope"] != "deploy")
    {
        throw ForbiddenException("wrong scope");
    }
}

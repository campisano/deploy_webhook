#include "httplib_http_deploy_controller.hpp"

#include <application/exceptions/invalid_deploy_exception.hpp>
#include <application/exceptions/deploy_error_exception.hpp>
#include <sstream>
#include <stdexcept>
#include "httplib_utils.hpp"
#include "../drivers/http/httplib_http_server.hpp"

// TODO [cmp] why there, and not in main, server.add([this]) doing that? why controller has ref to server and not vice-versa?
// generic controller could have path(/v1/deploy) and method(POST), maybe in the constructor so that you can still define they here, like
// : super("/v1/author", "POST")
HttplibHTTPDeployController::HttplibHTTPDeployController(
    HTTPServer & _server,
    const std::string & _algorithm,
    const std::string & _secret,
    DeployUsecasePort & _usecase,
    cmp::Logger & _logger)
    : m_algorithm(_algorithm), m_secret(_secret), m_usecase(_usecase),
      m_logger(_logger.namedLogger(__FILE__))
{
    HttplibHTTPServer & server = dynamic_cast<HttplibHTTPServer &>(_server);
    server.route(
        R"(/v1/deploy/(.+))",
        "POST",
        std::bind(
            &HttplibHTTPDeployController::deploy,
            this,
            std::placeholders::_1,
            std::placeholders::_2));
}

HttplibHTTPDeployController::~HttplibHTTPDeployController()
{
}

void HttplibHTTPDeployController::deploy(
    const httplib::Request & _request,
    httplib::Response & _response)
{
    try
    {
        std::stringstream msg;
        msg << "http request: path " << _request.path << ", method " << _request.method;
        m_logger->info(msg.str());
        m_logger->debug("body: " + _request.body);

        ensureAuth(getBearerToken(_request.get_header_value("authorization")),
                   m_algorithm,
                   m_secret,
                   *m_logger);

        auto stack_name = _request.matches[1];
        auto result = m_usecase.execute(stack_name, std::string(_request.body));

        setJSONResponse(_response, 200, {{"message", "deploy complete"}, {"result", result}});
    }
    catch(const UnauthorizedException & _exc)
    {
        m_logger->error(
            std::string("Unauthorized: ")
            + _exc.what());
        setJSONResponse(
            _response, 401,
        {{"message", "Unauthorized"}});
    }
    catch(const ForbiddenException & _exc)
    {
        m_logger->error(
            std::string("Forbidden: ")
            + _exc.what());
        setJSONResponse(
            _response, 403,
        {{"message", "Forbidden"}});
    }
    catch(const InvalidDeployException & _exc)
    {
        m_logger->error(
            std::string("Invalid deploy: ")
            + _exc.what());
        setJSONResponse(
            _response, 422,
        {{"message", "Invalid deploy"}, {"details", _exc.what()}});
    }
    catch(const DeployErrorException & _exc)
    {
        m_logger->error(
            std::string("Deploy error: ")
            + _exc.what());
        setJSONResponse(
            _response, 422,
        {{"message", "Deploy error"}, {"details", _exc.what()}});
    }
    catch(const std::exception & _exc)
    {
        m_logger->error(
            std::string("Internal server error: ")
            + _exc.what());
        setJSONResponse(
            _response, 500,
        {{"message", "Internal server error"}});
    }
}

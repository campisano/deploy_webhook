#ifndef HTTPLIB_UTILS__HPP__
#define HTTPLIB_UTILS__HPP__

#include <cmpsys/logger.hpp>
#include <httplib.h>
#include <map>
#include <stdexcept>
#include <string>

void setJSONResponse(
    httplib::Response & _response,
    int _status,
    std::map<std::string, std::string> _body);

std::string getBearerToken(const std::string & _auth_header);
void ensureAuth(const std::string & _token, const std::string & _algorithm,
                const std::string & _secret, cmp::Logger & _logger);

class UnauthorizedException : public std::runtime_error
{
public:
    virtual ~UnauthorizedException() = default;

public:
    explicit UnauthorizedException(const std::string & _message)
        : std::runtime_error(_message)
    {
    }
};



class ForbiddenException : public std::runtime_error
{
public:
    virtual ~ForbiddenException() = default;

public:
    explicit ForbiddenException(const std::string & _message)
        : std::runtime_error(_message)
    {
    }
};



#endif
